
// Test message
mp.events.addCommand('test', function (player) {
  player.outputChatBox("<p style='font-size: 30px; color: red; background-color: blue'>" + player.name + "| Message" + "</p>");
});

// me command 
mp.events.addCommand('me', function (player, fullText) {
  if (fullText === undefined) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /me [message] </p>")
  }

  else {
    player.outputChatBox("<p style='font-size: 15px; color: white; background-color: rgba(197, 77, 232, 0.5); width: 150%; height: 25px; border-radius: 3px; padding: 10px; margin-top: 10px;'> * me | " + player.name + " " + fullText);
  }
})

// do command
mp.events.addCommand('do', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /do [message] </p>")
  }

  else {
    player.outputChatBox("<p style='color: white; font-size: 15px; background-color: #985EFF; width: 150%; height: 25px; border-radius: 3px; padding: 10px; margin-top: 10px;'>" + player.name + " | " + fullText + "</p>")
  }
})

// admin command
mp.events.addCommand('admin', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /do [message] </p>")
  }

  else {
    player.outputChatBox("<p style='background-color: #FF352E; color: white; width: 150%; font-size: 15px; width: 150%; height: 25px; border-radius: 3px; padding: 10px; margin-top: 10px;'>"+ "Admin | " + fullText + "</p>")
  }
})

// ooc command
mp.events.addCommand('ooc', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /ooc [message] </p>")
  }

  else {
    player.outputChatBox("<p style='background-color: rgba(41, 41, 41, 0.6); color: white; width: 150%; height: 25px; margin-top: 10px; padding: 15px; border-radius: 3px;'>"+ "OOC | "+ player.name + " " + fullText + "</p>")
  }
})

// tweet command
mp.events.addCommand('tweet', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /tweet [message] </p>")
  }

  else {
    player.outputChatBox("<p style='background-color: rgba(28, 160, 242, 0.6); border-radius: 3px; padding: 15px; margin-top: 10px; height: 25px;'>"+ " Tweet | "+ player.name + " " + fullText +"</p>")
  }
})

// 911 command
mp.events.addCommand('911', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /911 [message] </p>")
  }
  
  else {
    player.outputChatBox("<p style='color: white; background-color: #375FFF; width: 150%; height: 25px; border-radius: 3px; padding: 15px; margin-top: 10px;'>"+"911 | " + fullText + "</p>")
  }
})

// Civil 
mp.events.addCommand('civil', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /civil [message] </p>")
  }
  
  else {
    player.outputChatBox("<p style='color: white; background-color: #8CEB2C; width: 150%; height: 25px; margin-top: 10px; padding: 15px; border-radius: 3px;'>"+"Civil | "+player.name + " " + fullText+"</p>")
  }
})

// Taxi 
mp.events.addCommand('taxi', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /taxi [message] </p>")
  }
  
  else {
    player.outputChatBox("<p style='color: white; background-color: #FFCF4D; width: 150%; height: 25px; margin-top: 10px; padding: 15px; border-radius: 3px;'>" + "Taxi | "+ player.name + " " + fullText + "</p>")
  }
})

// Uber
mp.events.addCommand('uber', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /uber [message] </p>")
  }
  
  else {
    player.outputChatBox("<p style='color: white; background-color: #000; width: 150%; height: 25px; margin-top: 10px; padding: 15px; border-radius: 3px;'>" + "Uber | "+ player.name + " " + fullText + "</p>")
  }
})

// Uber Eats
mp.events.addCommand('ubereats', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /ubereats [message] </p>")
  }
  
  else {
    player.outputChatBox("<p style='color: #47B275; background-color: #000; width: 150%; height: 25px; margin-top: 10px; padding: 15px; border-radius: 3px;'>" + "UberEats | "+ player.name + " " + fullText + "</p>")
  }
})

// Dispatch
mp.events.addCommand('dispatch', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /dispatch [message] </p>")
  }
  
  else {
    player.outputChatBox("<p style='color: white; background-color: #0F3BFF; width: 150%; height: 25px; margin-top: 10px; padding: 15px; border-radius: 3px;'>" + "Dispatch | " + " " + fullText + "</p>")
  }
})

// News
mp.events.addCommand('news', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /news [message] </p>")
  }
  
  else {
    player.outputChatBox("<p style='color: #F2F2F2; background-color: rgb(191, 31, 44, 0.8); width: 150%; height: 25px; margin-top: 10px; padding: 15px; border-radius: 3px;'>" + "News | " + " " + fullText + "</p>")
  }
})

// Ad
mp.events.addCommand('ad', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /ad [message] </p>")
  }
  
  else {
    player.outputChatBox("<p style='color: white; background-color: rgba(214, 168, 0, 1); width: 150%; height: 25px; margin-top: 10px; padding: 15px; border-radius: 3px;'>" + "Ad | "+ " " + fullText + "</p>")
  }
})

// Global
mp.events.addCommand('global', function (player, fullText) {
  if (fullText === undefined ) {
    player.outputChatBox("<p style='color: rgb(255, 39, 20); width: 150%;'>Something went wrong | Check spelling - Correct /global [message] </p>")
  }
  
  else {
    player.outputChatBox("<p style='color: white; background-color: rgba(34, 33, 34, 0.69); width: 150%; height: 25px; margin-top: 10px; padding: 15px; border-radius: 3px;'>" + "Global | "+ player.name + " " + fullText + "</p>")
  }
})

// When player don't use / with commands
mp.events.add('playerChat', function (player, fullText) {
  if (fullText === undefined ) {
    return
  }

  else {
    player.outputChatBox("<p style='color: white; background-color: #757473; width: 150%; height: 25px; margin-top: 10px; padding: 15px; border-radius: 3px;'>"+player.name + " " + fullText+ "</p>")
  }
  
})
