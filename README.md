# RAGEMP Colored chat 
This is resource for ragemp that adds colored boxes around commands like '/me or /do'

## Instalation
* Copy all resource folders to your game folders. 
* Add `require('./colored_chat/index)` to your root `index.js` in server folder in `path_to_RAGEMP/server-files/packages/index`
* Add `require('./colored_chat_client/index)` to your root `index.js` client folder in `path_to_RAGEMP/server-files/client_packages`
* Lauch the game and check out how it works

# Screenshots
![Screen1](docs/screen1.png)
![Screen2](docs/screen2.png)
![Screen3](docs/screen3.png)
![Screen4](docs/screen4.png)
![Screen5](docs/screen5.png)

## Description
Last red text shows when user enter command but without any text after the command. 


# Customization
There is a chance to customize this resource for your own purpose. Just edit the main `colored_chat` file 

# Adding new commands
```javascript
// Template
mp.events.addCommand('test', function (player) {
  player.outputChatBox("<p style='font-size: 30px; color: red; background-color: blue'>" + player.name + "| Message" + "</p>");
});
```
To add new commands just add above snippet to your `server-files/colored_chat/index.js` and replace test with new command and add new style or change existed after the style tag 
